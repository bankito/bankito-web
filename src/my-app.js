/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import './my-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Menu</app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <template is="dom-if" if="{{!isSession}}">
            <a name="view-login" href="[[rootPath]]view-login">Login</a>
            <a name="view-register" href="[[rootPath]]view-register">Registrar</a>
            </template>
            <a name="view-home" href="[[rootPath]]view-home" hidden="true">Home</a>
            <template is="dom-if" if="{{isSession}}">
            <a name="view-persona" href="[[rootPath]]view-persona">Mi cuenta</a>
            <a name="view-movimientos" href="[[rootPath]]view-movimientos">Movimientos</a>
            <a on-click="logout">Cerrar Sessión</a>
            </template>
          </iron-selector>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="">Bankito</div>
            </app-toolbar>
          </app-header>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <view-login name="view-login"></view-login>
            <view-register name="view-register"></view-register>
            <view-register-success name="view-register-success"></view-register-success>
            <view-resumen name="view-resumen"></view-resumen>
            <view-home name="view-home"></view-home>
            <view-paso1 name="view-paso1"></view-paso1>
            <view-paso2 name="view-paso2"></view-paso2>
            <view-paso3 name="view-paso3"></view-paso3>
            <view-persona-cuenta name="view-persona-cuenta"></view-persona-cuenta>
            <view-persona name="view-persona"></view-persona>
            <view-movimientos name="view-movimientos"></view-movimientos>
            <my-view404 name="view404"></my-view404>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      isSession: {
        type: Boolean,
        notify: true, 
        value() {
          let user = window.atob(sessionStorage.getItem("user"));
          let token = sessionStorage.getItem("token");
          if(user == undefined){
            return false;
          }
          if(token == undefined){
            return false;
          }
          return true;
        }
      }
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view-login' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'view-home';
    } else if (['view-login', 'view-register', 'view3','view-register-success','view-resumen','view-home', 'view-paso1', 'view-paso2', 'view-paso3', 'view-persona-cuenta', 'view-persona', 'view-movimientos'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'view-login':
        import('./view-login.js');
        break;
      case 'view-register':
        import('./view-register.js');
        break;
      case 'view-register-success':
        import('./view-register-success.js');
        break;
      case 'view-resumen':
        import('./view-resumen.js');
        break;
      case 'view-home':
        import('./view-home.js');
        break;
      case 'view-paso1':
        import('./view-paso1.js');
        break;
      case 'view-paso2':
        import('./view-paso2.js');
        break;
      case 'view-paso3':
        import('./view-paso3.js');
        break;
      case 'view-persona-cuenta':
        import('./view-persona-cuenta.js');
        break;
      case 'view-persona':
        import('./view-persona.js');
        break;
      case 'view-movimientos':
        import('./view-movimientos.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }

  logout(){
    sessionStorage.clear();
    window.location.href="view-home";
  }

}

window.customElements.define('my-app', MyApp);
