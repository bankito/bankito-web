/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-radio-button/paper-radio-button.js';
import '@polymer/paper-radio-group/paper-radio-group.js';

class ViewHome extends PolymerElement {
  static get template() {
    return html`
        <style include="shared-styles">
            :host {
            display: block;

            padding: 10px;
            }
        </style>
        <div class="card">
          <h3>Bienvenido a Bankito</h3>
          <p>Bankito, es una plataforma que busca mejorar la forma en que se realizan las operaciones de cambio de moneda actualmente, optimizando los tiempos y reduciendo los peligros que implican el traslado de dinero.</p>
          <hr>
          <div>
          Tipo de cambio ($)<br>
          <p>Compra: S/ {{dolar_compra}} | Venta: S/ {{dolar_venta}}</p>
          </div>
          <hr>
          <paper-radio-group selected="{{tipoOperacion}}" >
              <paper-radio-button name="compra">Comprar</paper-radio-button>
              <paper-radio-button name="venta">Vender</paper-radio-button>
          </paper-radio-group>
          <div id="dvComprar" style="display: none;">
          <paper-input id="enviasComprar" label="[[enviasLabel]]" placeholder="[[enviasPlaceholder]]" always-float-label value="{{dolar_compra_tengo::input}}" auto-validate allowed-pattern="[0-9]" >
          <div slot="prefix">S/ </div>
          </paper-input>
          <paper-input id="recibesComprar" label="[[recibesLabel]]" placeholder="[[recibesPlaceholder]]" always-float-label value="[[dolar_compra_recibo]]" disabled>
          <div slot="prefix">$ </div>
          </paper-input>
          </div>
          <div id="dvVender" style="display: none;">
          <paper-input id="enviasVender" label="[[enviasLabel]]" placeholder="[[enviasPlaceholder]]" always-float-label value="{{dolar_venta_tengo::input}}" auto-validate allowed-pattern="[0-9]" >
          <div slot="prefix">$ </div>
          </paper-input>
          <paper-input id="recibesVender" label="[[recibesLabel]]" placeholder="[[recibesPlaceholder]]" always-float-label value="[[dolar_venta_recibo]]" disabled>
          <div slot="prefix">S/ </div>
          </paper-input>
          </div>
          <template is="dom-if" if="{{isSession}}">
          <a href='[[rootPath]]view-paso1' style="text-decoration: none">
          <paper-button raised class="custom green" >Cambiar</paper-button>
          </a>
          </template>
          <template is="dom-if" if="{{!isSession}}">
          <a href='[[rootPath]]view-login' style="text-decoration: none">
          <paper-button raised class="custom green" >Cambiar</paper-button>
          </a>
          </template>
        </div>
    `;
  }
  static get properties() {
    return {
        dolar_compra: { type: String, notify: true, value() {
            var url = "http://34.203.247.134:8080/moneda/convert_usd";
            var request = new XMLHttpRequest();
            request.open("GET", url, false);
            request.setRequestHeader("Accept","application/json");
            request.send();
            return JSON.parse(request.responseText).compra;
        } },
        dolar_venta: { type: String, notify: true, value() {
            var url = "http://34.203.247.134:8080/moneda/convert_usd";
            var request = new XMLHttpRequest();
            request.open("GET", url, false);
            request.setRequestHeader("Accept","application/json");
            request.send();
            return JSON.parse(request.responseText).venta;
        } },
        dolar_venta_tengo: {type: Number, notify: true, observer: 'calcular_cambio_venta'},
        dolar_venta_recibo: { type: Number, notify: true},
        dolar_compra_tengo: {type: Number, notify: true, observer: 'calcular_cambio_compra'},
        dolar_compra_recibo: { type: Number, notify: true},
        enviasPlaceholder: { type: String, value: " Tu envias" },
        recibesPlaceholder: { type: String, value: " Tu recibes" },
        enviasLabel: { type: String, value: "Envias" },
        recibesLabel: { type: String, value: "Recibes" },
        tipoOperacionLabel: {type: String, value: "Escoja una operación"},
        tipoOperacion: {type: String, observer: '_observer'},
        isSession: {
          type: Boolean,
          notify: true, 
          value() {
            let user = window.atob(sessionStorage.getItem("user"));
            let token = sessionStorage.getItem("token");
            if(user == undefined){
              return false;
            }
            if(token == undefined){
              return false;
            }
            return true;
          }
        }
    }
  }

  calcular_cambio_venta(){
    this.dolar_venta_recibo = (this.dolar_venta_tengo*this.dolar_venta).toFixed(2);
  }

  calcular_cambio_compra(){
    this.dolar_compra_recibo = (this.dolar_compra_tengo/this.dolar_compra).toFixed(2);
  }

  _observer(){
    if(this.tipoOperacion == "compra"){
      this.shadowRoot.getElementById('dvComprar').style.display="";
      this.shadowRoot.getElementById('dvVender').style.display="none";
      this.dolar_venta_recibo = "";
      this.dolar_venta_tengo = "";
    }else if(this.tipoOperacion == "venta"){
      this.shadowRoot.getElementById('dvComprar').style.display="none";
      this.shadowRoot.getElementById('dvVender').style.display="";
      this.dolar_compra_recibo = "";
      this.dolar_compra_tengo = "";
    }else{
      this.shadowRoot.getElementById('dvComprar').style.display="none";
      this.shadowRoot.getElementById('dvVender').style.display="none";
    }
  }
}

window.customElements.define('view-home', ViewHome);
