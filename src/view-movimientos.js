/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';

class ViewMovimientos extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <div class="card">
        <h3>Mis movimientos</h3>
        <template is="dom-repeat" items="{{movimientos}}" as="movimiento">
        <div>
          <div>[[movimiento.tipo_operacion]]</div>
          <div>$ [[movimiento.compra_recibi]][[movimiento.venta_deposite]] a S/ [[movimiento.tasa_cambio]]</div>
        </div>
        <hr/>
        </template>
        <template is="dom-if" if="[[_isEqualTo(movimientos, '')]]">
        <div>No tiene cuentas registradas</div>
        </template>
      </div>
    `;
  }

  static get properties() {
    return {
      movimientos: { type: String, notify: true, value() {
        let user = window.atob(sessionStorage.getItem("user"));
        var url = "http://34.203.247.134:8080/movimiento/"+user;
        var request = new XMLHttpRequest();
        request.open("GET", url, false);
        request.setRequestHeader("Accept","application/json");
        request.setRequestHeader("x-access-token",sessionStorage.getItem("token"));
        request.send();
        return JSON.parse(request.responseText).movimientos;
      } }
    }
  }

  _isEqualTo(title, string) {
    return title == string;
  }

}

window.customElements.define('view-movimientos', ViewMovimientos);
