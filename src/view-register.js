/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import {PolymerElement} from '@polymer/polymer/polymer-element.js';
import {html} from '@polymer/polymer/lib/utils/html-tag.js';

import './shared-styles.js';
import '@polymer/iron-selector/iron-selector.js';

class ViewRegister extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      paper-button.custom {
        --paper-button-ink-color: var(--paper-pink-a200);
        /* These could also be individually defined for each of the
          specific css classes, but we'll just do it once as an example */
        --paper-button-flat-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
        --paper-button-raised-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
      }
      paper-button.custom:hover {
        background-color: var(--paper-indigo-100);
      }
      paper-button.green {
        background-color: var(--paper-green-500);
        color: white;
      }
      paper-button.green[active] {
        background-color: var(--paper-red-500);
      }
      </style>
      <div class="card">
        <h3>Registrar</h3>
        <div class="msg_error" inner-H-T-M-L="[[msgError]]"></div>
        <form id="form" is="iron-form">
        <paper-input id="username" label="[[userLabel]]" placeholder="[[userPlaceholder]]" always-float-label value="{{username::input}}" required auto-validate error-message="Ingrese un usuario" >
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="mail" label="[[mailLabel]]" placeholder="[[mailPlaceholder]]" always-float-label value="{{mail::input}}" required auto-validate error-message="Ingrese un email" >
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="password" label="[[passwordLabel]]" placeholder="[[passwordPlaceholder]]" type="password" always-float-label value="{{password::input}}" required auto-validate error-message="Ingrese su contraseña" >
          <iron-icon icon="icons:lock"></iron-icon>
        </paper-input>
        <paper-input id="rePassword" label="[[rePasswordLabel]]" placeholder="[[rePasswordPlaceholder]]" type="password" always-float-label value="{{repassword::input}}" required auto-validate error-message="Ingrese su contraseña">
          <iron-icon icon="icons:lock"></iron-icon>
        </paper-input>
        <label for="tipoDocumento">Selecione su documento</label>
        <select name="tipoDocumento" id="tipoDocumento" value="{{tipoDocumento::change}}">
          <option selected>[[tipoDocumentoLabel]]</option>
          <option value="dni">DNI</option>
          <option value="ruc">RUC</option>
        </select>
        <div id="dvDNI" style="display: none;">
        <paper-input id="dni" label="[[dniLabel]]" placeholder="[[dniPlaceholder]]" always-float-label value="{{dni::input}}" required auto-validate allowed-pattern="[0-9]" error-message="Ingrese un DNI" on-keyup="checkDNI">
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="nombre" label="[[nombreLabel]]" always-float-label required value="{{nombre::input}}" disabled>
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="apellido" label="[[apellidoLabel]]" always-float-label required value="{{apellido::input}}" disabled>
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        </div>
        <div id="dvRUC" style="display: none;">
        <paper-input id="ruc" label="[[rucLabel]]" placeholder="[[rucPlaceholder]]" always-float-label value="{{ruc::input}}" required auto-validate allowed-pattern="[0-9]" error-message="Ingrese un RUC" on-keyup="checkRUC">
            <iron-icon icon="social:person"></iron-icon>
          </paper-input>
        <paper-input id="razonSocial" label="[[razonSocialLabel]]" always-float-label required value="{{razonSocial::input}}" disabled>
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        </div>
        <div>
        <paper-button raised class="custom green" on-tap="_register" >Registrar</paper-button>
        </div>
        </form>
      </div>
      <iron-ajax
        id="doRegister"
        url="http://34.203.247.134:8080/usuario"
        method="POST"
        handle-as="json"
        content-type=application/json
        on-response="_onResponse"
        on-error="_onError"
        >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      userLabel: { type: String, value: "Usuario" },
      passwordLabel: { type: String, value: "Contraseña" },
      rePasswordLabel: { type: String, value: "Confirmar contraseña" },
      mailLabel: { type: String, value: "Email" },
      razonSocialLabel: { type: String, value: "Razon Social" },
      nombreLabel: { type: String, value: "Nombre" },
      apellidoLabel: { type: String, value: "Apellido" },
      tipoDocumentoLabel: { type: String, value: "Tipo de Documento" },
      dniLabel: { type: String, value: "DNI" },
      rucLabel: { type: String, value: "RUC" },
      userPlaceholder: { type: String, value: "Ingrese su usuario" },
      passwordPlaceholder: { type: String, value: "Ingrese su contraseña" },
      rePasswordPlaceholder: { type: String, value: "Confirme su contraseña" },
      mailPlaceholder: { type: String, value: "Ingrese email" },
      nombrePlaceholder: { type: String, value: "Ingrese su nombre" },
      apellidoPlaceholder: { type: String, value: "Ingrese su apellido" },
      dniPlaceholder: { type: String, value: "Ingrese su DNI" },
      rucPlaceholder: { type: String, value: "Ingrese su RUC" },
      createButtonText: { type: String, value: "Crear" },
      msgError: { type: String, value: "" },
      tipoDocumento:{type: String, observer: '_observer'}
    }
  }

  _register(){
    console.log(this.username);
    if(this.username == undefined){
      this.msgError = "Ingrese su usuario";
      return;
    }
    if(this.password == undefined){
      this.msgError = "Ingrese su contraseña";
      return;
    }
    if(this.repassword == undefined){
      this.msgError = "Confirme su contraseña";
      return;
    }
    if(this.repassword != this.password){
      this.msgError = "Las contraseñas ingresadas no coinciden";
      return;
    }
    if(this.mail == undefined){
      this.msgError = "Ingrese su email";
      return;
    }
    if(this.tipoDocumento == undefined){
      this.msgError = "Escoja un documento";
      return;
    }

    if(this.tipoDocumento == "dni"){
      if(this.dni == undefined || this.dni == ""){
        this.msgError = "Ingrese su DNI";
        return;
      }
    }
    if(this.tipoDocumento == "ruc"){
      if(this.ruc == undefined || this.ruc == ""){
        this.msgError = "Ingrese su RUC";
        return;
      }
    }

    let user = {
      "username": this.username,
      "password": this.password,
      "mail": this.mail,
      "tipoDocumento": this.tipoDocumento,
      "dni": this.dni,
      "nombre": this.nombre,
      "apellido": this.apellido,
      "ruc": this.ruc,
      "razonSocial": this.razonSocial
    }

    this.$.doRegister.body = JSON.stringify(user);
    this.$.doRegister.generateRequest();
  }

  _onError(e,request) {
    this.msgError = "Ocurrio un error"
  }

  _onResponse (e, request){
    var response = request.response;
    console.log(response)
    if(response.ok == false){
      this.msgError = response.msg.errmsg;
    }else{
      window.location.href="view-register-success";
    }
  }

  _observer(){
    if(this.tipoDocumento == "dni"){
      this.shadowRoot.getElementById('dvDNI').style.display="";
      this.shadowRoot.getElementById('dvRUC').style.display="none";
      this.ruc = "";
      this.razonSocial = "";
    }else if(this.tipoDocumento == "ruc"){
      this.shadowRoot.getElementById('dvDNI').style.display="none";
      this.shadowRoot.getElementById('dvRUC').style.display="";
      this.dni = "";
      this.nombre = "";
      this.apellido = "";
    }else{
      this.shadowRoot.getElementById('dvDNI').style.display="none";
      this.shadowRoot.getElementById('dvRUC').style.display="none";
    }
  }

  checkDNI(){
    if(this.dni.length == 8){
      var url = "http://34.203.247.134:8080/persona/dni/"+this.dni;
      var request = new XMLHttpRequest();
      request.open("GET", url, false);
      request.setRequestHeader("Accept","application/json");
      request.send();
      this.nombre = JSON.parse(request.responseText).nombres;
      this.apellido = JSON.parse(request.responseText).apellidos;
    }
      
  }

  checkRUC(){
    if(this.ruc.length == 11){
      var url = "http://34.203.247.134:8080/persona/ruc/"+this.ruc;
      var request = new XMLHttpRequest();
      request.open("GET", url, false);
      request.setRequestHeader("Accept","application/json");
      request.send();
      this.razonSocial = JSON.parse(request.responseText).razon_social;
    }
      
  }

}

window.customElements.define('view-register', ViewRegister);