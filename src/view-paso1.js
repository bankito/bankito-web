/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-radio-button/paper-radio-button.js';
import '@polymer/paper-radio-group/paper-radio-group.js';

class ViewPaso1 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <div class="card">
        <div class="circle">1</div>
        <h3>Paso 1</h3>
        <div>
        Tipo de cambio ($)<br>
        <p>Compra: S/ {{dolar_compra}} | Venta: S/ {{dolar_venta}}</p>
        </div>
        <hr>
        <paper-radio-group selected="{{tipoOperacion}}" >
            <paper-radio-button name="compra">Comprar</paper-radio-button>
            <paper-radio-button name="venta">Vender</paper-radio-button>
        </paper-radio-group>
        <div id="dvComprar" style="display: none;">
        <paper-input id="enviasComprar" label="[[enviasLabel]]" placeholder="[[enviasPlaceholder]]" always-float-label value="{{dolar_compra_tengo::input}}" auto-validate allowed-pattern="[0-9]" >
        <div slot="prefix">S/ </div>
        </paper-input>
        <paper-input id="recibesComprar" label="[[recibesLabel]]" placeholder="[[recibesPlaceholder]]" always-float-label value="[[dolar_compra_recibo]]" disabled>
        <div slot="prefix">$ </div>
        </paper-input>
        </div>
        <div id="dvVender" style="display: none;">
        <paper-input id="enviasVender" label="[[enviasLabel]]" placeholder="[[enviasPlaceholder]]" always-float-label value="{{dolar_venta_tengo::input}}" auto-validate allowed-pattern="[0-9]" >
        <div slot="prefix">$ </div>
        </paper-input>
        <paper-input id="recibesVender" label="[[recibesLabel]]" placeholder="[[recibesPlaceholder]]" always-float-label value="[[dolar_venta_recibo]]" disabled>
        <div slot="prefix">S/ </div>
        </paper-input>
        </div>
        <br/>
        <label for="cuenta">Recibiré mi dinero en:</label>
        <select name="cuenta" id="cuenta" value="{{cuenta_sel::change}}" style="font-family: 'Roboto', 'Noto', sans-serif;font-size:14px;">
          <option value="">Escoge tu cuenta bancaria</option>
          <template is="dom-repeat" items="{{cuentas}}" as="cuenta">
          <option value="[[cuenta.alias]]">[[cuenta.alias]]</option>
          </template>
        </select>
        <a href="[[rootPath]]view-persona-cuenta">Agregar cuenta</a>
        <br/><br/>
        <paper-button raised class="custom green" on-click="siguiente">Siguiente</paper-button>
      </div>
    `;
  }
  static get properties() {
    return {
        dolar_compra: { type: String, notify: true, value() {
            var url = "http://34.203.247.134:8080/moneda/convert_usd";
            var request = new XMLHttpRequest();
            request.open("GET", url, false);
            request.setRequestHeader("Accept","application/json");
            request.send();
            return JSON.parse(request.responseText).compra;
        } },
        dolar_venta: { type: String, notify: true, value() {
            var url = "http://34.203.247.134:8080/moneda/convert_usd";
            var request = new XMLHttpRequest();
            request.open("GET", url, false);
            request.setRequestHeader("Accept","application/json");
            request.send();
            return JSON.parse(request.responseText).venta;
        } },
        cuentas: { type: String, notify: true, value() {
          let user = window.atob(sessionStorage.getItem("user"));
          var url = "http://34.203.247.134:8080/cuenta/"+user;
          var request = new XMLHttpRequest();
          request.open("GET", url, false);
          request.setRequestHeader("Accept","application/json");
          request.setRequestHeader("x-access-token",sessionStorage.getItem("token"));
          request.send();
          return JSON.parse(request.responseText).cuentas;
        } },
        dolar_venta_tengo: {type: Number, notify: true, observer: 'calcular_cambio_venta'},
        dolar_venta_recibo: { type: Number, notify: true},
        dolar_compra_tengo: {type: Number, notify: true, observer: 'calcular_cambio_compra'},
        dolar_compra_recibo: { type: Number, notify: true},
        enviasPlaceholder: { type: String, value: " Tu envias" },
        recibesPlaceholder: { type: String, value: " Tu recibes" },
        enviasLabel: { type: String, value: "Envias" },
        recibesLabel: { type: String, value: "Recibes" },
        tipoOperacionLabel: {type: String, value: "Escoja una operación"},
        tipoOperacion: {type: String, observer: '_observer'}
    }
  }
  calcular_cambio_venta(){
    this.dolar_venta_recibo = (this.dolar_venta_tengo*this.dolar_venta).toFixed(2);
  }

  calcular_cambio_compra(){
    this.dolar_compra_recibo = (this.dolar_compra_tengo/this.dolar_compra).toFixed(2);
  }

  _observer(){
    if(this.tipoOperacion == "compra"){
      this.shadowRoot.getElementById('dvComprar').style.display="";
      this.shadowRoot.getElementById('dvVender').style.display="none";
      this.dolar_venta_recibo = "";
      this.dolar_venta_tengo = "";
    }else if(this.tipoOperacion == "venta"){
      this.shadowRoot.getElementById('dvComprar').style.display="none";
      this.shadowRoot.getElementById('dvVender').style.display="";
      this.dolar_compra_recibo = "";
      this.dolar_compra_tengo = "";
    }else{
      this.shadowRoot.getElementById('dvComprar').style.display="none";
      this.shadowRoot.getElementById('dvVender').style.display="none";
    }
  }

  siguiente(){
    sessionStorage.setItem('tipoOperacion', this.tipoOperacion);
    sessionStorage.setItem('cuenta_sel', window.btoa(this.cuenta_sel));
    if(this.tipoOperacion == "compra"){
      sessionStorage.setItem('dolar_compra_tengo', window.btoa(this.dolar_compra_tengo));
      sessionStorage.setItem('dolar_compra_recibo', window.btoa(this.dolar_compra_recibo));
      sessionStorage.setItem('dolar_compra', window.btoa(this.dolar_compra));
    }else if(this.tipoOperacion == "venta"){
      sessionStorage.setItem('dolar_venta_tengo', window.btoa(this.dolar_venta_tengo));
      sessionStorage.setItem('dolar_venta_recibo', window.btoa(this.dolar_venta_recibo));
      sessionStorage.setItem('dolar_venta', window.btoa(this.dolar_venta));
    }
    window.location.href="view-paso2";
  }
}

window.customElements.define('view-paso1', ViewPaso1);
