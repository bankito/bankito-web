/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import {PolymerElement} from '@polymer/polymer/polymer-element.js';
import {html} from '@polymer/polymer/lib/utils/html-tag.js';

import './shared-styles.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-radio-button/paper-radio-button.js';
import '@polymer/paper-radio-group/paper-radio-group.js';

class ViewPersonaCuenta extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      paper-button.custom {
        --paper-button-ink-color: var(--paper-pink-a200);
        /* These could also be individually defined for each of the
          specific css classes, but we'll just do it once as an example */
        --paper-button-flat-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
        --paper-button-raised-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
      }
      paper-button.custom:hover {
        background-color: var(--paper-indigo-100);
      }
      paper-button.green {
        background-color: var(--paper-green-500);
        color: white;
      }
      paper-button.green[active] {
        background-color: var(--paper-red-500);
      }
      </style>
      <div class="card">
        <h3>Nueva cuenta</h3>
        <div class="msg_error" inner-H-T-M-L="[[msgError]]"></div>
        <form id="form" is="iron-form">
        <paper-input id="alias" label="[[aliasLabel]]" placeholder="[[aliasPlaceholder]]" always-float-label value="{{alias::input}}" required auto-validate error-message="Ingrese un alias" >
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <label for="tipoCuenta">Moneda</label>
        <paper-radio-group id="tipoCuenta" selected="{{tipoCuenta}}" >
          <paper-radio-button name="soles">Soles (S/)</paper-radio-button>
          <paper-radio-button name="dolares">Dolares ($)</paper-radio-button>
        </paper-radio-group>
        <paper-input id="cuenta" label="[[cuentaLabel]]" placeholder="[[cuentaPlaceholder]]" always-float-label value="{{cuenta::input}}" required auto-validate error-message="Ingrese su cuenta" >
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <label for="tipoDocumento">Propietario de la cuenta</label>
        <paper-radio-group id="tipoDocumento" selected="{{tipoDocumento}}" >
          <paper-radio-button name="dni">DNI</paper-radio-button>
          <paper-radio-button name="ruc">RUC</paper-radio-button>
        </paper-radio-group>
        <div id="dvDNI" style="display: none;">
        <paper-input id="dni" label="[[dniLabel]]" placeholder="[[dniPlaceholder]]" always-float-label value="{{dni::input}}" required auto-validate allowed-pattern="[0-9]" error-message="Ingrese un DNI" on-keyup="checkDNI">
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="nombre" label="[[nombreLabel]]" always-float-label required value="{{nombre::input}}" disabled>
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="apellido" label="[[apellidoLabel]]" always-float-label required value="{{apellido::input}}" disabled>
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        </div>
        <div id="dvRUC" style="display: none;">
        <paper-input id="ruc" label="[[rucLabel]]" placeholder="[[rucPlaceholder]]" always-float-label value="{{ruc::input}}" required auto-validate allowed-pattern="[0-9]" error-message="Ingrese un RUC" on-keyup="checkRUC">
            <iron-icon icon="social:person"></iron-icon>
          </paper-input>
        <paper-input id="razonSocial" label="[[razonSocialLabel]]" always-float-label required value="{{razonSocial::input}}" disabled>
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        </div>
        <div>
        <paper-button raised class="custom green" on-tap="_register" >Registrar</paper-button>
        </div>
        </form>
      </div>
      <iron-ajax
        id="doRegister"
        url="http://34.203.247.134:8080/cuenta"
        method="POST"
        handle-as="json"
        content-type=application/json
        on-response="_onResponse"
        on-error="_onError"
        >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      aliasLabel: { type: String, value: "Alias" },
      cuentaLabel: { type: String, value: "Cuenta" },
      razonSocialLabel: { type: String, value: "Razon Social" },
      nombreLabel: { type: String, value: "Nombre" },
      apellidoLabel: { type: String, value: "Apellido" },
      tipoDocumentoLabel: { type: String, value: "Tipo de Documento" },
      dniLabel: { type: String, value: "DNI" },
      rucLabel: { type: String, value: "RUC" },
      aliasPlaceholder: { type: String, value: "Ingrese su alias" },
      cuentaPlaceholder: { type: String, value: "Ingrese su cuenta" },
      nombrePlaceholder: { type: String, value: "Ingrese su nombre" },
      apellidoPlaceholder: { type: String, value: "Ingrese su apellido" },
      dniPlaceholder: { type: String, value: "Ingrese su DNI" },
      rucPlaceholder: { type: String, value: "Ingrese su RUC" },
      createButtonText: { type: String, value: "Crear" },
      msgError: { type: String, value: "" },
      tipoDocumento:{type: String, observer: '_observer'}
    }
  }

  _register(){
    if(this.alias == undefined){
      this.msgError = "Ingrese su alias";
      return;
    }
    if(this.tipoCuenta == undefined){
      this.msgError = "Escoja la moneda de la cuenta";
      return;
    }
    if(this.cuenta == undefined){
      this.msgError = "Ingrese su cuenta";
      return;
    }
    if(this.tipoDocumento == undefined){
      this.msgError = "Escoja un documento";
      return;
    }

    if(this.tipoDocumento == "dni"){
      if(this.dni == undefined || this.dni == ""){
        this.msgError = "Ingrese el DNI asociado a la cuenta";
        return;
      }
    }
    if(this.tipoDocumento == "ruc"){
      if(this.ruc == undefined || this.ruc == ""){
        this.msgError = "Ingrese el RUC asociado a la cuenta";
        return;
      }
    }

    let cuenta = {
      "user": window.atob(sessionStorage.getItem("user")),
      "alias": this.alias,
      "cuenta": this.cuenta,
      "tipoCuenta": this.tipoCuenta,
      "tipoDocumento": this.tipoDocumento,
      "dni": this.dni,
      "nombre": this.nombre,
      "apellido": this.apellido,
      "ruc": this.ruc,
      "razonSocial": this.razonSocial
    }
    console.log(this.headers)

    this.$.doRegister.body = JSON.stringify(cuenta);
    this.$.doRegister.headers['x-access-token'] = sessionStorage.getItem("token");
    this.$.doRegister.generateRequest();
  }

  _onError(e,request) {
    console.log(e);
    this.msgError = "Ocurrio un error"
  }

  _onResponse (e, request){
    var response = request.response;
    console.log(response)
    if(response.ok == false){
      this.msgError = response.msg.errmsg;
    }else{
      window.location.href="view-persona";
    }
  }

  _observer(){
    if(this.tipoDocumento == "dni"){
      this.shadowRoot.getElementById('dvDNI').style.display="";
      this.shadowRoot.getElementById('dvRUC').style.display="none";
      this.ruc = "";
      this.razonSocial = "";
    }else if(this.tipoDocumento == "ruc"){
      this.shadowRoot.getElementById('dvDNI').style.display="none";
      this.shadowRoot.getElementById('dvRUC').style.display="";
      this.dni = "";
      this.nombre = "";
      this.apellido = "";
    }else{
      this.shadowRoot.getElementById('dvDNI').style.display="none";
      this.shadowRoot.getElementById('dvRUC').style.display="none";
    }
  }

  checkDNI(){
    if(this.dni.length == 8){
      var url = "http://34.203.247.134:8080/persona/dni/"+this.dni;
      var request = new XMLHttpRequest();
      request.open("GET", url, false);
      request.setRequestHeader("Accept","application/json");
      request.send();
      this.nombre = JSON.parse(request.responseText).nombres;
      this.apellido = JSON.parse(request.responseText).apellidos;
    }
      
  }

  checkRUC(){
    if(this.ruc.length == 11){
      var url = "http://34.203.247.134:8080/persona/ruc/"+this.ruc;
      var request = new XMLHttpRequest();
      request.open("GET", url, false);
      request.setRequestHeader("Accept","application/json");
      request.send();
      this.razonSocial = JSON.parse(request.responseText).razon_social;
    }
      
  }

}

window.customElements.define('view-persona-cuenta', ViewPersonaCuenta);