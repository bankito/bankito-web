/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import {PolymerElement} from '@polymer/polymer/polymer-element.js';
import {html} from '@polymer/polymer/lib/utils/html-tag.js';

import './shared-styles.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';


class ViewPersona extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      paper-button.custom {
        --paper-button-ink-color: var(--paper-pink-a200);
        /* These could also be individually defined for each of the
          specific css classes, but we'll just do it once as an example */
        --paper-button-flat-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
        --paper-button-raised-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
      }
      paper-button.custom:hover {
        background-color: var(--paper-indigo-100);
      }
      paper-button.green {
        background-color: var(--paper-green-500);
        color: white;
      }
      paper-button.green[active] {
        background-color: var(--paper-red-500);
      }
      </style>
      <div class="card">
        <h3>Datos de usuario</h3>
        <paper-input id="username" label="[[userLabel]]" always-float-label value="{{usuario.user}}" disabled>
        </paper-input>
        <paper-input id="mail" label="[[mailLabel]]" always-float-label value="{{usuario.email}}" disabled >
        </paper-input>
        <paper-input id="dni" label="[[tipoDocumentoLabel]]" always-float-label value="{{usuario.id_documento_identidad}}" disabled>
        </paper-input>
        <paper-input id="dni" label="[[numeroDocumentoLabel]]" always-float-label value="{{usuario.numero_documento}}" disabled>
        </paper-input>
        <template is="dom-if" if="[[_isEqualTo(usuario.id_documento_identidad, 'dni')]]">
        <paper-input id="nombre" label="[[nombreLabel]]" always-float-label value="{{usuario.first_name}}" disabled>
        </paper-input>
        <paper-input id="apellido" label="[[apellidoLabel]]" always-float-label value="{{usuario.last_name}}" disabled>
        </paper-input>
        </template>
        <template is="dom-if" if="[[_isEqualTo(usuario.id_documento_identidad, 'ruc')]]">
        <paper-input id="razonSocial" label="[[razonSocialLabel]]" always-float-label value="{{usuario.razon_social}}" disabled>
        </paper-input>
        </template>
        <hr/>
        <h3>Cuentas</h3>
        <template is="dom-repeat" items="{{cuentas}}" as="cuenta">
        <div>
          <div>[[cuenta.alias]]</div>
          <div>Cuenta BBVA [[cuenta.moneda]]: [[cuenta.cuenta]]</div>
          <hr/>
        </div>
        </template>
        <template is="dom-if" if="[[_isEqualTo(cuentas, '')]]">
        <div>No tiene cuentas registradas</div>
        </template>
        <br/>
        <div>
        <a href="[[rootPath]]view-persona-cuenta" style="text-decoration: none">
        <paper-button raised class="custom green" on-tap="_register" >Agregar cuenta</paper-button>
        </a>
        </div>
      </div>
    `;
  }

  static get properties() {
    return {
      usuario: { type: String, notify: true, value() {
        let user = window.atob(sessionStorage.getItem("user"));
        var url = "http://34.203.247.134:8080/usuario/"+user;
        var request = new XMLHttpRequest();
        request.open("GET", url, false);
        request.setRequestHeader("Accept","application/json");
        request.setRequestHeader("x-access-token",sessionStorage.getItem("token"));
        request.send();
        return JSON.parse(request.responseText).usuario;
      } },
      cuentas: { type: String, notify: true, value() {
        let user = window.atob(sessionStorage.getItem("user"));
        var url = "http://34.203.247.134:8080/cuenta/"+user;
        var request = new XMLHttpRequest();
        request.open("GET", url, false);
        request.setRequestHeader("Accept","application/json");
        request.setRequestHeader("x-access-token",sessionStorage.getItem("token"));
        request.send();
        return JSON.parse(request.responseText).cuentas;
      } },
      userLabel: { type: String, value: "Usuario" },
      mailLabel: { type: String, value: "Email" },
      razonSocialLabel: { type: String, value: "Razon Social" },
      nombreLabel: { type: String, value: "Nombre" },
      apellidoLabel: { type: String, value: "Apellido" },
      tipoDocumentoLabel: { type: String, value: "Tipo de Documento" },
      numeroDocumentoLabel: { type: String, value: "Numero de Documento" },
      nombrePlaceholder: { type: String, value: "Ingrese su nombre" },
      apellidoPlaceholder: { type: String, value: "Ingrese su apellido" }
    }
  }

  _isEqualTo(title, string) {
    return title == string;
  }
}

window.customElements.define('view-persona', ViewPersona);