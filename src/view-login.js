/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class ViewLogin extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      paper-button.custom {
        --paper-button-ink-color: var(--paper-pink-a200);
        /* These could also be individually defined for each of the
          specific css classes, but we'll just do it once as an example */
        --paper-button-flat-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
        --paper-button-raised-keyboard-focus: {
          background-color: var(--paper-pink-a200) !important;
          color: white !important;
        };
      }
      paper-button.custom:hover {
        background-color: var(--paper-indigo-100);
      }
      paper-button.green {
        background-color: var(--paper-green-500);
        color: white;
      }
      paper-button.green[active] {
        background-color: var(--paper-red-500);
      }
      </style>
      <div class="card">
        <h3>Login</h3>
        <div class="msg_error" inner-H-T-M-L="[[msgError]]"></div>
        <paper-input id="username" label="[[userLabel]]" placeholder="[[userPlaceholder]]" always-float-label required value="{{username::input}}">
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-input id="password" label="[[passwordLabel]]" placeholder="[[passwordPlaceholder]]" type="password" always-float-label required value="{{password::input}}">
          <iron-icon icon="icons:lock"></iron-icon>
        </paper-input>
        <paper-button raised class="custom green" on-click="_login">Entrar</paper-button>
      </div>
      <iron-ajax
        id="doLogin"
        url="http://34.203.247.134:8080/login"
        method="POST"
        handle-as="json"
        content-type=application/json
        on-response="_onResponse"
        on-error="_onError"
        >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      userLabel: { type: String, value: "Usuario" },
      passwordLabel: { type: String, value: "Contraseña" },
      userPlaceholder: { type: String, value: "Ingrese su usuario" },
      passwordPlaceholder: { type: String, value: "Ingrese su contraseña" },
      loginButtonText: { type: String, value: "Login" },
      logoutButtonText: { type: String, value: "Logout" },
      msgError: { type: String, value: "" }
    }
  }

  _login(){
    let login = {
      "username": this.username,
      "password": this.password,
    }

    this.$.doLogin.body = JSON.stringify(login);
    this.$.doLogin.generateRequest();
  }

  _onError(e,request) {
    this.msgError = "Ocurrio un error"
  }

  _onResponse (e, request){
    var response = request.response;
    console.log(response.login)
    if(response.auth == false){
      this.msgError = response.msg;
    }else{
      sessionStorage.setItem('token', response.token);
      sessionStorage.setItem('user', window.btoa(this.username));
      window.location.href="view-home";
    }
  }

}

window.customElements.define('view-login', ViewLogin);