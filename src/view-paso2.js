/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class ViewPaso2 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <div class="card">
        <div class="circle">2</div>
        <h3>Paso 2</h3>
        <p>Transfiere {{moneda}} {{monto}} a nuestra cuenta:</p>
        <ul>
        <li type="disc">Banco: BBVA</li>
        <li type="disc">Tipo de cuenta: Corriente</li>
        <li type="disc">Número de cuenta: XXXXXXXXXXXX</li>
        <li type="disc">Codigo de Cuenta Interbancario: XXXXXXXXXXXXXXX</li>
        <li type="disc">Titular de la cuenta: Bankito S.A.C.</li>
        <li type="disc">RUC: XXXXXXXXX</li>
        </ul>
        <p>Necesitamos validar tu operación por favor ingrese el número de operación</p>
        <paper-input id="numOperacion" label="[[numOperacionLabel]]" placeholder="[[numOperacionPlaceholder]]" always-float-label required value="{{numOperacion::input}}">
          <iron-icon icon="social:person"></iron-icon>
        </paper-input>
        <paper-button raised class="custom green" on-click="validar">Validar</paper-button>
      </div>
      <iron-ajax
        id="doVerificar"
        url="http://34.203.247.134:8080/movimiento"
        method="POST"
        handle-as="json"
        content-type=application/json
        on-response="_onResponse"
        on-error="_onError"
        >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      numOperacionLabel: { type: String, value: "Número de operación" },
      numOperacionPlaceholder: { type: String, value: "Ingrese el número de operación" },
      moneda: {type: String, notify: true, value() {
        let tipoOperacion = sessionStorage.getItem("tipoOperacion");
        if(tipoOperacion == "compra"){
          console.log("compra")
          return "S/"
        }else{
          console.log("venta")
          return "$"
        }
      }},
      monto: {type: String, notify: true, value() {
        let tipoOperacion = sessionStorage.getItem("tipoOperacion");
        if(tipoOperacion == "compra"){
          return window.atob(sessionStorage.getItem("dolar_compra_tengo"));
        }else{
          return window.atob(sessionStorage.getItem("dolar_venta_tengo"));
        }
      }}
    }
  }

  validar(){
    let operacion = {
      "user": window.atob(sessionStorage.getItem("user")),
      "numOperacion": this.numOperacion,
      "cuenta_sel": window.atob(sessionStorage.getItem("cuenta_sel")),
      "tipoOperacion": sessionStorage.getItem("tipoOperacion"),
      "dolar_venta_tengo": window.atob(sessionStorage.getItem("dolar_venta_tengo")),
      "dolar_venta_recibo": window.atob(sessionStorage.getItem("dolar_venta_recibo")),
      "dolar_compra_recibo": window.atob(sessionStorage.getItem("dolar_compra_recibo")),
      "dolar_compra_tengo": window.atob(sessionStorage.getItem("dolar_compra_tengo")),
      "dolar_compra": window.atob(sessionStorage.getItem("dolar_compra")),
      "dolar_venta": window.atob(sessionStorage.getItem("dolar_venta"))
    }

    this.$.doVerificar.body = JSON.stringify(operacion);
    this.$.doVerificar.headers['x-access-token'] = sessionStorage.getItem("token");
    this.$.doVerificar.generateRequest();
  }

  _onError(e,request) {
    this.msgError = "Ocurrio un error"
  }

  _onResponse (e, request){
    var response = request.response;
    if(response.auth == false){
      this.msgError = response.msg;
    }else{
      
      window.location.href="view-paso3";
    }
  }

}

window.customElements.define('view-paso2', ViewPaso2);
