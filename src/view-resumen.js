/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class ViewResumen extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <div class="card">
        <h1>Bienvenido a Bankito</h1>
        <p>Mediante Bankito podrás comprar y vender</p>

        <p>Compra: S/ {{dolar_compra}} | Venta: S/ {{dolar_venta}}</p>
        <hr style="color: #0056b2;"/>
        <p>Quiero vender dolares</p>
        <paper-input label="Tengo" type="number" value="{{dolar_venta_tengo::input}}" always-float-label>
          <div slot="prefix">$ </div>
        </paper-input>
        <paper-input label="Recibo" type="number" value="[[dolar_venta_recibo]]" always-float-label disabled>
        <div slot="prefix">S/ </div>
        </paper-input>
        <button>Vender</button>
        <hr style="color: #0056b2;"/>
        <p>Quiero comprar dolares</p>
        <paper-input label="Tengo" type="number" value="{{dolar_compra_tengo::input}}" always-float-label>
          <div slot="prefix">S/ </div>
        </paper-input>
        <paper-input label="Recibo" type="number" value="[[dolar_compra_recibo]]" always-float-label disabled>
        <div slot="prefix">$ </div>
        </paper-input>
        <button>Comprar</button>
      </div>

      <!-- <iron-ajax
        id="tipo_cambio"
        url="http://34.203.247.134:8080/moneda/convert_usd"
        method="POST"
        handle-as="json"
        content-type=application/json
        on-response="_onResponse"
        on-error="_onError"
        >
      </iron-ajax> -->
    `;
  }

  static get properties() {
    return {
      // dolar_compra: { type: String, notify: true, value() {
      //   var url = "http://34.203.247.134:8080/moneda/convert_usd";
      //   var request = new XMLHttpRequest();
      //   request.open("GET", url, false);
      //   request.setRequestHeader("Accept","application/json");
      //   request.send();
      //   return JSON.parse(request.responseText).compra;
      // } },
      // dolar_venta: { type: String, notify: true, value() {
      //   var url = "http://34.203.247.134:8080/moneda/convert_usd";
      //   var request = new XMLHttpRequest();
      //   request.open("GET", url, false);
      //   request.setRequestHeader("Accept","application/json");
      //   request.send();
      //   return JSON.parse(request.responseText).venta;
      // } }
      dolar_compra: { type: Number, notify: true, value: 3.355},
      dolar_venta: { type: Number, notify: true, value: 3.302},
      dolar_venta_tengo: {type: Number, notify: true, observer: 'calcular_cambio_venta'},
      dolar_venta_recibo: { type: Number, notify: true},
      dolar_compra_tengo: {type: Number, notify: true, observer: 'calcular_cambio_compra'},
      dolar_compra_recibo: { type: Number, notify: true}
    }
  }

  calcular_cambio_venta(){
    this.dolar_venta_recibo = this.dolar_venta_tengo*this.dolar_venta;
  }

  calcular_cambio_compra(){
    this.dolar_compra_recibo = this.dolar_compra_tengo/this.dolar_compra;
  }

  // ready() {
  //   // super.ready();
  //   // this.$.tipo_cambio.generateRequest();
  // }

  // _onError(e,request) {
  //   this.msgError = "Ocurrio un error"
  // }

  // _onResponse (e, request){
  //   var response = request.response;
  //   console.log(response.login)
  //   if(response.ok == false){
  //     this.msgError = response.msg;
  //   }else{
  //     sessionStorage.setItem('token', response.token);
  //     window.location.href="view2";
  //   }
  // }
}

window.customElements.define('view-resumen', ViewResumen);
