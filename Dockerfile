FROM node:11

LABEL maintainer="andresgarcia.kaox@gmail.com"

RUN npm install -g polymer-cli --unsafe-perm

WORKDIR /app

COPY package.json .

RUN npm install --quiet

COPY . .

RUN polymer build

EXPOSE 8081

CMD ["polymer", "serve", "-p", "8081", "-H", "0.0.0.0"]